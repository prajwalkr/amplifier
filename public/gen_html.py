import sys, os

from shutil import copy

# title
title = 'meeting15'

# audio element
video_file = '<video controls width="300" height="360"> <source src="{}" type="video/mp4"> </video>'

dest_folder = './files/{}/'.format(title)

# Start creating data, first row is headers
data = [['Serial No.', 'Kinetics-700 Label', 'Sample 1', 'Sample 2', 'Sample 3', 'Pose Keypoints (showing best one of the three)']]

for num, label in enumerate(os.listdir(dest_folder + 'videos/')):
	row = [num + 1, label]

	for i in range(3):
		row.append(dest_folder + '/videos/' + label + '/{}.mp4'.format(i))

	pose_estimate_folder = dest_folder + '/poses/' + label
	if not os.path.exists(pose_estimate_folder):
		row.append('Pose estimation failed completely')

	else:
		row.append(dest_folder + '/poses/' + label + '/{}'.format(os.listdir(pose_estimate_folder)[0]))

	data.append(row)


#### creates table after this. 

fileout = open("{}.html".format(title), "w")

table = '<head><title>{}</title><link href="styles/style.css" rel="stylesheet" type="text/css" ></head><body><table>\n'.format(title)

# Create the table's column headers
header = data[0]
table += "  <tr>\n"
for column in header:
	table += "    <th>{0}</th>\n".format(column.strip())
table += "  </tr>\n"

# Create the table's row data
for row in data[1:]:
	table += "  <tr>\n"
	table += "    <td>{}</td>\n".format(row[0])
	table += "    <td>{}</td>\n".format(row[1])

	for column in row[2:5]:
		table += "    <td>{}</td>\n".format(video_file.format(column))

	if row[5] == 'Pose estimation failed completely': table += "    <td><p>{}</p></td>\n".format(row[5])
	else:
		print(video_file.format(row[5]))
		table += "    <td>{}</td>\n".format(video_file.format(row[5]))

	table += "  </tr>\n"

table += "</table></body>"

fileout.writelines(table)
fileout.close()