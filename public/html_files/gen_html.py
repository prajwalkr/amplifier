import sys, os

from shutil import copy

# title
title = 'meeting11'

# audio element
audio_file = '<audio controls><source src="{}" type="audio/wav">Your browser does not support the audio element.</audio>'

# specify source and destination folders for files
src_folder = '../../results/'
dest_folder = './files/{}/'.format(title)

if not os.path.exists(dest_folder):
	os.mkdir(dest_folder)


# Start creating data, first row is headers
data = [['Repeated Pulse', 'Mixture @ 0.1x', 'Prediction @ 0.1x', 'Mixture @ 0.01x', 'Prediction @ 0.01x']]

column_formatters = ['{}_gt.wav', '{}_0.1_inp.wav', '{}_0.1_pred_cp.wav', '{}_0.01_inp.wav', '{}_0.01_pred_cp.wav']

for i in range(32):
	row = []

	if not os.path.exists(src_folder + column_formatters[0].format(i)): continue

	for c in column_formatters:
		if not os.path.exists(src_folder + c.format(i)):
			raise IOError('Some parts of one row missing!')

		row.append(dest_folder + c.format(i))
		copy(src_folder + c.format(i), dest_folder + c.format(i))

	data.append(row)


#### creates table after this. 

fileout = open("{}.html".format(title), "w")

table = '<head><title>{}</title><link href="styles/style.css" rel="stylesheet" type="text/css" ></head><body><table>\n'.format(title)

# Create the table's column headers
header = data[0]
table += "  <tr>\n"
for column in header:
    table += "    <th>{0}</th>\n".format(column.strip())
table += "  </tr>\n"

# Create the table's row data
for row in data[1:]:
    table += "  <tr>\n"
    for column in row:
        table += "    <td>{}</td>\n".format(audio_file.format(column))
    table += "  </tr>\n"

table += "</table></body>"

fileout.writelines(table)
fileout.close()